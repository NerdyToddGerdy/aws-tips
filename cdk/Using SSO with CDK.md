
# Using SSO with cdk

For Mac:
1. we will be using homebrew
   1. `brew install pipx`
2. We will be using pipx
   1. `pipx ensurepath`
   2. `pipx install aws-sso-credentials-process`
   3. `pipx install aws-export-credentials`
3. Make sure you are using the aws cli v2
4. add the following to .zshrc
   ```
    export AWS_CONFIGURE_SSO_DEFAULT_SSO_START_URL=`AWS SSO  URL`/start
    export AWS_CONFIGURE_SSO_DEFAULT_SSO_REGION=us-east-1
    sso(){
        unset AWS_PROFILE
        export AWS_PROFILE=$1
        aws sts get-caller-identity &> /dev/null || aws sso login || (unset AWS_PROFILE && aws-configure-sso-profile --profile $1)
        eval $(aws-export-credentials --env-export)
    }
    ```
5. in new cli:
    `sso <insert sso profile name here>-<insert account name here>`
